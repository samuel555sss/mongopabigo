class Author
  include Mongoid::Document
  field :name
  field :_id, type: String, default: ->{ name } # which was `key :name` before v3
  has_many :articles
  
end
